const Discord = require("discord.js");
const db = require('../lib/db')
class AnimeController {
    enviarAleatorio = async (message, args) => {
        if (args.length == 0) {
            const anime = await db.query('select * from animes order by random() limit 1')
            const rows = anime.rows
            const animeEmbed = new Discord.MessageEmbed()
                .setTitle(rows[0].nome)
                .addField('Ano: ', rows[0].ano, true)
                .addField('Autor: ', rows[0].autor, true )
                .addField('Editora: ', rows[0].editora, true )
                .addField('Estudio: ', rows[0].estudio, true )
                .addField('Estado: ', rows[0].estado, true )
                .addField('Epsodios: ', rows[0].epsodios, true )
                .addField('Generos: ', rows[0].generos, true )
                .setURL('https://animesonline.vip/?s='+escape(rows[0].nome).replace(/%20/g, "+"))
                .setFooter(`Categoria: ${rows[0].generos}`)
                .attachFiles(['src/assets/images/'+ rows[0].capa])
                .setImage(`attachment://${rows[0].capa}`)
            await message.channel.send(animeEmbed)
        

        // } else if (args[0].toLowerCase() == 'categoria') {
        //     const categorias = await db.query('SELECT array_to_string(array_agg(distinct "categoria"),$$ -|- $$) AS categoria  from piadas')
        //     message.channel.send(categorias.rows[0].categoria)
         } else if (args.length == 1) {
            const anime = await db.query('select * from animes where lower(generos::text)::text[] @> ARRAY[$1] order by random() limit 1',[args[0].toLowerCase()])
            const rows = anime.rows
            const animeEmbed = new Discord.MessageEmbed()
                
                .setTitle(rows[0].nome)
                .addField('Ano: ', rows[0].ano, true)
                .addField('Autor: ', rows[0].autor, true )
                .addField('Editora: ', rows[0].editora, true )
                .addField('Estudio: ', rows[0].estudio, true )
                .addField('Estado: ', rows[0].estado, true )
                .addField('Epsodios: ', rows[0].epsodios, true )
                .addField('Generos: ', rows[0].generos, true )
                .setURL('https://animesonline.vip/?s='+escape(rows[0].nome).replace(/%20/g, "+"))
                .setFooter(`Categoria: ${rows[0].generos}`)
                .attachFiles(['src/assets/images/'+ rows[0].capa])
                .setImage(`attachment://${rows[0].capa}`)
            await message.channel.send(animeEmbed)
         }

        console.log(args)
    }
}

module.exports = new AnimeController()
