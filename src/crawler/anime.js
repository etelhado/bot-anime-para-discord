const axios = require("axios");
const cheerio = require("cheerio");
const Path = require("path");
var uuid = require("uuid");
const db = require("../lib/db");
const Fs = require("fs");

var first_page = 1;
var baseUrl = "https://www.anitube.cx/anime/page/";

async function delay(ms) {
  return await new Promise((resolve) => setTimeout(resolve, ms));
}

async function downloadImage(imgUrl, name) {
  const url = imgUrl;
  const path = Path.resolve("src", "assets", "images", `${name}`);
  const writer = Fs.createWriteStream(path);
  await delay(2000);
  const response = await axios({
    url,
    method: "GET",
    responseType: "stream",
  });

  response.data.pipe(writer);

  return new Promise((resolve, reject) => {
    writer.on("finish", resolve);
    writer.on("error", reject);
  });
}

const pegaAnimes = () => {
  console.log("Iniciando Cranlwe");
  delay(2000).then(() => {
    axios(baseUrl + first_page)
      .then((response) => {
        const html = response.data;
        const $ = cheerio.load(html);
        const tabDiv = $(".boxThumbnail");
        tabDiv.each((index, element) => {
          var aName = $(element).children(".item_content").find(".go").text();
          console.log("Nome da porra do anime: ", aName);
          const bolds = $(element).find("a").attr("href");
          const animeUrl = "https://www.anitube.cx" + bolds;
          axios(animeUrl).then((animeRes) => {
            console.log("pegando anime");
            const img_final_name = uuid.v4() + ".jpg";
            const $a = cheerio.load(animeRes.data);
            var sinopse = $a('.well-sm').find('span').text()
            const pnBody = $a(".panel-body");
            const animeInfo = pnBody
              .children(".advancedEpisodeList")
              .children(".col-md-8 ");
            const imgAnime = pnBody
              .children(".advancedEpisodeList")
              .children(".col-md-4")
              .children("img")
              .prop("src");

            downloadImage(imgAnime, img_final_name).then((s) => {
              console.log("Imagem Baixada");
            });
            
            const paragraph = animeInfo.find("p");
            const numEp = $a(paragraph[1]).text().split(":")[1].trim();
            const anAno = $a(paragraph[2]).text().split(":")[1].trim();
            const aAuthor = $a(paragraph[3]).text().split(":")[1].trim();
            const aEditora = $a(paragraph[4]).text().split(":")[1].trim();
            const aEstudio = $a(paragraph[5]).text().split(":")[1].trim();
            const aStatus = $a(paragraph[6]).text().split(":")[1].trim();
            const aGeneros = $a(paragraph[7])
              .text()
              .split(":")[1]
              .trim()
              .split(",");
            const querySql =
              "INSERT INTO public.animes " +
              "(nome, epsodios, ano, autor, editora, estudio, estado, generos, capa, descricao) " +
              "VALUES " +
              "($1, $2, $3, $4, $5, $6, $7, $8, $9, $10)";
            console.log("tentando salvar");
            db.query(querySql, [
              aName,
              numEp,
              anAno,
              aAuthor,
              aEditora,
              aEstudio,
              aStatus,
              aGeneros,
              img_final_name,
              sinopse.trim()
            ])
              .then((s) => {
                console.log("registro inserido");
              })
              .catch((e) => console.error(e));
          });
        });
        first_page++;
        pegaAnimes();
      })
      .catch((error) => {
        console.log("parou em ", error);
      });
  });
};

pegaAnimes();
